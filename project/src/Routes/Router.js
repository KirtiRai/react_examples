import React from "react"
// console.log(React.version, "moni");
import { Routes, Route } from "react-router-dom"
import ScreenHome from "../ScreenHome/ScreenHome"
import SignUp from "../ScreenSignUp/SignUp"
import ScreenEmailInput from "../ScreenSignUp/ScreenEmailInput"
import ScreenSignUpOtpVerify from "../ScreenSignUp/ScreenSignUpOtpVerify"
import PrivacyPolicyComponent from "../ScreenPrivacyPolicy/PrivacyPolicyComponent"
import TermsConditionComponent  from "../ScreenTermsConditions/TermsConditionComponent"
import Footer from "../CommonComponent/FooterComponent/Footer"
import ScreenBrandProduct from "../ScreenBrand/ProductBrands"
import ScreenHomeTopBrands from "../ScreenHome/ScreenHomeTopBrandComponent/ScreenHomeTopBrands"
import Cart from "../ScreenBrand/Cart"
import NavBarLower from "../CommonComponent/NavBarLowerComponent/NavBarLower"
const Router = () => {
  return (
    <Routes>
      <Route exact path="/" element={ <ScreenHome/> } />     
      <Route path="/auth" element={ <SignUp/> } />
      <Route path="/brand" element={<ScreenBrandProduct />} />
      <Route path="/cart" element={ <Cart /> } />
      <Route path="/navbar" element={ <NavBarLower /> } />
      <Route path="/ScreenBrand" element={<ScreenHomeTopBrands />} />
      <Route path="/ptype-auth" element={ <ScreenEmailInput/> } />
      <Route path="/auth" element={ <ScreenSignUpOtpVerify/> } />
      <Route path="/privacy-policy" element={ <PrivacyPolicyComponent/> } />
      <Route path="/terms-conditions" element={ <TermsConditionComponent/> } />
      <Route path="/footer" element={ <Footer/> } />
    </Routes>
  )
}

export default Router
