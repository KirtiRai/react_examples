export default {

  //upperNavbar Localization..............//
  Beauty:"BEAUTY BONAZA Get Your Daily Of Dose Amazing Deals",
  getApp:"GetApp",
  store:"Store & Events",
  giftCard:"Gift Card",
  help:"Help",

  // middleNavbar Localization.................//
  Categories:"Categories",
  Brands:"Brands",
  NykaFashion:"Nykaa Fashion",
  BeautyAdvice:"Beauty Advice",
  NykaaNetwork:"Nykaa Network",
  SearchNykaa:"Search on Nykaa",
  Account:"Account",
  changeLanguage: "Change your langauge",

  // lowerNavbar Localization...........................//

  Makeup:"Makeup",
  Skin:"Skin",
  Hair:"Hair",
  Appliances:"Appliances",
  PersonalCare:"PersonalCare",
  Natural:"Natural",
  MomBaby:"Mom & Baby",
  HealthWellness:"Health & Wellness",
  Men:"Men",
  Fragrance:"Fragrance",
  Luxe:"LUXE"

    
};
  