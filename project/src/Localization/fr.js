export default {
  //upperNavbar Localization..............//
  Beauty:"BEAUTY BONITA Recevez votre dose quotidienne ",
  getApp:"GetApp",
  store:"Magasin & Evénements",
  giftCard:"Cartecadeau",
  help:"Aider",


  // middleNavbar Localization.................//
  Categories:"Catégories",
  Brands:"Marques",
  NykaFashion:"Nykaa Fashion",
  BeautyAdvice:"Conseil Beauté",
  NykaaNetwork:"Nykaa Network",
  SearchNykaa:"Recherche sur Nykaa",
  Account:"Compte",

  // lowerNavbar Localization...........................//

  Makeup:"Maquillage",
  Skin:"Peau",
  Hair:"Cheveux",
  Appliances:"Appareils",
  PersonalCare:"Soins personnels",
  Natural:"Naturelle",

  MomBaby:"Maman et bébé",
  HealthWellness:"Santé",
  Men:"Hommes",
  Fragrance:"Fragrance",
  Luxe:"LUXE"

};
  