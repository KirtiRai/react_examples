import React from "react"
import NavBarUpper from "../CommonComponent/NavBarUpperComponent/NavBarUpper"
import NavBarMiddle from "../CommonComponent/NavBarMiddleComponent/MiddleComponent"
import NavBarLower from "../CommonComponent/NavBarLowerComponent/NavBarLower"
import Footer from "../CommonComponent/FooterComponent/Footer"
import "./StyleTermsConditions.css"

const TermsConditionComponent = () => {
  return (
    <div className='grey-background'>
      <NavBarUpper />
      <NavBarMiddle />
      <NavBarLower />
      <div className='section'>
        <h2 className='terms-condition-heading'>TERMS & CONDITIONS</h2>
        <div className='content'>
          <h4>1. USE OF THE PLATFORM </h4>
          <p className='para-font-size'>Welcome to www.nykaa.com (<strong>"Site" or "Nykaa "</strong>). The website www.nykaa.com is owned and operated by Nykaa E-Retail Private Limited
            ("<strong>NYKAA</strong>"), and the retail stores i.e. Nykaa Luxe and Nykaa On Trend is operated under FSN Brands Marketing Private Limited ("FSN B")
            a company incorporated under the provisions of the Companies Act, 2013 with its registered office at 104, Vasan Udyog Bhavan, Sun Mill Compound, S.B.Marg,
            Tulsi Pipe Road, Lower Parel, Mumbai 400 013 India. Nykaa and FSN B are together referred to as NYKAA and the Site and retail stores are together referred
            as Platform. You may be accessing our Site from a computer or mobile phone device (through an iOS or Android application, for example) and these Terms of
            Use govern your use of our Site and your conduct, regardless of the means of access. You may also be using our interactive services ("<strong>Interactive Services</strong> "),
            such as our Beauty Suggestions, Beauty Book etc. and the Beauty Services wherein you can book any beauty service provided by our beauty partners ("<strong>Beauty Services</strong>")
            and these Terms of Use also govern your use of the Interactive and Beauty Services. These Terms of Use govern all the products offered on the Platform including the Interactive and Beauty Services.<br />
            The Platform is only to be used for your personal non-commercial use and information. Your use of the services and features of the Platform shall be governed by these Terms and Conditions (hereinafter "
          <strong>Terms of Use</strong> ") along with the Privacy Policy, Shipping Policy and Cancellation, Refund and Return Policy (together "
          <strong>Policies</strong>") as modified and amended from time to time.<br />
            By mere accessing or using the Platform, you are acknowledging, without limitation or qualification, to be bound by these Terms of Use and the Polices, whether you have read the same or not.
          <strong>ACCESSING, BROWSING OR OTHERWISE USING THE PLATFORM INDICATES YOUR UNCONDITIONAL AGREEMENT TO ALL THE TERMS AND CONDITIONS IN THIS AGREEMENT, SO PLEASE READ THIS AGREEMENT CAREFULLY BEFORE PROCEEDING
          </strong> . If you do not agree to any of the terms enumerated in the Terms of Use or the Policies, please do not use the Platform. You are responsible to ensure that your access to this Platform and material
            available on or through it are legal in each jurisdiction, in or through which you access or view the platform or such material.
          <br />NYKAA reserves the unilateral right to change the particulars contained in the Terms of Use or the Policies from time to time and at any time, without notice to its users and in its sole discretion.
            If NYKAA decides to change the Terms of Use or Policies, NYKAA will post the new version of the Terms of Use or the Policies on the Site and update the date specified above. Any change or modification to
            the Terms of Use and the Policies will be effective immediately from the date of such upload of the Terms of Use and Policies on the Site. Your continued use of the Platform following the modifications to
            the Terms of Use and Policies constitutes your acceptance of the modified Terms of Use and Policies whether or not you have read them. For this reason, you should frequently review these Terms of Use, our
            Guidelines and Rules and any other applicable policies, including their dates, to understand the terms and conditions that apply to your use of the Site.</p>

          <h4>2. PRIVACY PRACTICES </h4>
          <p className='para-font-size'>We understand the importance of safeguarding your personal information and we have formulated a Privacy Policy, to ensure that your personal information is sufficiently protected. Apart from these Terms of Use,
            the Privacy Policy shall also govern your visit and use of the Site. Your continued use of the Site implies that you have read and accepted the Privacy Policy and agree to be bound by its terms and conditions.
            You consent to the use of personal information by NYKAA in accordance with the terms of and purposes set forth in the Privacy Policy, the same may be subject to amendment from time to time at the sole discretion of NYKAA.</p>

          <h4>3. YOUR ACCOUNT </h4>
          <p className='para-font-size'>This Site is directed to be used by adults only. We assume that any minor, if at all, accessing our Site is under the supervision of their guardians. NYKAA or its associates do not knowingly collect information from minors.
            You will be responsible for maintaining confidentiality of your account, password, and restricting access to your computer, and you hereby accept responsibility for all activities that occur under your account and password.
            You acknowledge that the information you provide, in any manner whatsoever, are not confidential or proprietary and does not infringe any rights of a third party in whatsoever nature</p>
          <p>We reserve the right to refuse service and/or terminate accounts without prior notice if these Terms of Use are violated or if we decide, in our sole discretion, that it would be in Nykaa's and NYKAA's best interests to do so.
            You are solely responsible for all contents that you upload, post, email or otherwise transmit via the Site.The information provided to us shall be maintained by us in accordance with our Privacy Policy.</p>

          <h4>4. PRODUCT & SERVICES INFORMATION </h4>
          <p className='para-font-size'>NYKAA attempts to be as accurate as possible in the description of the product on the Platform. However, NYKAA does not warrant that the product description, colour, information or other content of the Platform is accurate,
            complete, reliable, current or error-free. The Site may contain typographical errors or inaccuracies and may not be complete or current. The product pictures are indicative and may not match the actual product.</p>

          <h4>5. PRODUCT USE & SERVICES </h4>
          <p className='para-font-size'>The products and services available on the Platform, and the samples, if any, that Platform may provide you, are for your personal and/or professional use only. The products or services, or samples thereof, which you may receive
            from us, shall not be sold or resold for any/commercial reasons.</p>
          <p className='para-font-size'>In case any products or beauty services purchased / received / availed causes side effects or doesn't suit you, please note that NYKAA is in no manner responsible for any manufactural side-effects or service provider and manufacturer
            or service provider of the product or service shall be solely responsible for such side effects and consumer complaints. You should carefully read the individual terms and conditions in relation to the products and beauty services and
            consult a specialist before the use of the same.</p>

          <h4>6. RECOMMENDATION OF THE PRODUCT & SERVICES </h4>
          <p className='para-font-size'>Any recommendation made to you in the Site during the course of your use of the Site is purely for informational purposes and for your convenience and does not amount to endorsement of the product or services by NYKAA or any of
            its associates in any manner.</p>

          <h4>7. CANCELLATIONS, REFUNDS AND RETURNS </h4>
          <p className='para-font-size'>Please refer to our <a href="cancellation-policy">Cancellation, Refunds and Returns Policy</a> provided on our Site.</p>

          <h4>8. MODE OF PAYMENT </h4>
          <p className='para-font-size'>Payments for the products available on the Site may be made in the following ways:</p>
          <ul className='para-font-size'>
            <li>Payments can be made by Credit Cards, Debit Cards, Net Banking, Wallets, e-Gift cards, and reward points.</li>
            <li>Credit card, Debit Card and Net Banking payment options are instant payment options and recommended to ensure faster processing of your order.</li>
            <li>Cash On Delivery option is not available for orders outside India.</li>
          </ul>

          <h4>9. SHIPPING AND DELIVERY </h4>
          <p className='para-font-size'>Please refer to our <a href="/shipping-policy">Shipping and Delivery Policy</a> provided on our Site, as amended from time to time.</p>

          <h4>10. CHAT FUNCTIONALITY </h4>
          <p className='para-font-size'>The Chat Functionality has been provided to help you with any and all Site related queries. Any use of this service shall be subject to the following conditions:</p>
          <ul className='para-font-size'>
            <li>NYKAA may suspend the chat service at any time without notice.</li>
            <li>NYKAA or its executives are not responsible for any delay caused in attending to or replying to the queries via chat.</li>
            <li>Communication through chat may be stored by NYKAA for future reference, and the user of such service will not have the right to access such information at any future date.</li>
            <li>While 'chatting' you may not communicate any objectionable information i.e. unlawful, threatening, abusive, defamatory, obscene information.</li>
            <li>The chat room shall not be used to sell any products, to give suggestion on business opportunity or any other form of solicitation.</li>
            <li>You may proceed further and chat with our online customer care executive only if you agree to the above terms and conditions.</li>
          </ul>

          <h4>11. CONTACT INFORMATION: </h4>
          <p className='para-font-size'><strong>Customer Service Desk</strong><br />
            E-mail id:<a href="mailto:support@nykaa.com">support@nykaa.com</a><br />
            E-mail id (for international customers):<a href="mailto:support@nykaa.com"> internationalsupport@nykaa.com</a><br />
            Phone: 1800-267-4444<br />
          </p>
          <p className='para-font-size'>Contact Days: Monday to Saturday (8:00 a.m. to 10:00 p.m.)<br />Sunday (10.00 a.m. to 7.00 p.m.)</p>
        </div>
      </div>
      <Footer />
    </div>
  )
}

export default TermsConditionComponent