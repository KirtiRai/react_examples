import React from "react"
import NavBarUpper from "../CommonComponent/NavBarUpperComponent/NavBarUpper"
import NavBarMiddle from "../CommonComponent/NavBarMiddleComponent/NavBarMiddle"
import NavBarLower from "../CommonComponent/NavBarLowerComponent/NavBarLower"
import Footer from "../CommonComponent/FooterComponent/Footer"
import "./StylePrivacyPolicy.css"

const PrivacyPolicyComponent = () => {
  return (
    <div className='grey-background'>
      <NavBarUpper />
      <NavBarMiddle />
      <NavBarLower />
      <div className='section'>
        <h2 className='policy-heading'>NYKAA BEAUTY PRIVACY POLICY </h2>
        <div className='content'>
          <p className='para-font-size'>This Privacy Policy outlines Nykaa E-Retail Private Limited's approach to Data Protection and Privacy to fulfil its obligations under the applicable laws and regulations. 
          This Privacy Policy applies to your Personal Data which is processed by us, whether in physical or electronic mode. While you may be able to browse the platform (Website 
          and App collectively referred to as “Platform”) from countries outside of India, however please note we do not offer any product/service under this Platform outside India.  
          By visiting the platform or providing your information, you expressly agree to be bound by this Privacy Policy and agree to be governed by the laws of India including but 
          not limited to the laws applicable to data protection and privacy. If you do not agree please do not use or access our Platform.In this Privacy Policy, the expressions 
          'Personal Data', 'Data Subject', 'Controller', 'Processor' and 'Processing' shall have the meanings given to them in the applicable privacy laws .</p>
          <p className='para-font-size'>We are committed to treating data privacy seriously. It is important that you know exactly what we do with your Personal Data.</p>
          <p className='para-font-size'>Throughout this document, “we”, “us”, “our”, “ours” refer to NYKAA E-RETAIL PRIVATE LIMITED . Wherever we have said 'you' or 'your', this means YOU</p>
          
          <h4>WHO WE ARE </h4>
          <p className='para-font-size'><strong>NYKAA E-RETAIL PRIVATE LIMITED </strong>is a company incorporated and registered under the provisions of the Companies Act, 2013 and having its registered office at 104, 
          Vasan Udyog Bhavan, Sun Mill Compound, Lower Parel, Mumbai 400 013. <strong>Nykaa E-Retail Private Limited </strong>is engaged in the business of facilitating selling, marketing and 
          retailing <strong>cosmetic and beauty products </strong>(“Business”) through the e-commerce websites and mobile applications (“App”) both developed and owned by <strong>Nykaa E-Retail 
          Private Limited </strong>and/or its parent company, and/or its affiliates (Website and App collectively referred to as “Platform”) or offline stores / events to conduct its Business.</p>
          
          <h4> ROLES WE PLAY </h4>
          <p className='para-font-size'>We play the role of a Data Controller when we collect and process Personal Data about you.</p>
          <p className='para-font-size'>We play the role of a Data Processor when we collect and process Personal Data on behalf of another Data Controller</p>
          
          <h4>OUR COMMITMENT </h4>
          <p className='para-font-size'>We commit to protecting your privacy and hence our Personal Data handling practices are continually reviewed to ensure compliance with the 
            <strong>applicable Privacy laws and regulations</strong></p>
          
          <h4>PERSONAL INFORMATION GATHERED BY NYKAA </h4>
          <p className='para-font-size'>The information we learn and gather from you, personal or otherwise, is used to register you, verify your identity to permit you to use the app, undertake transactions 
          (including to facilitate and process payments), communicate with you,convey any promotional offers, services or updates associated with NYKAA, and generally maintain your 
          accounts with us. We also use this information to customize your experience and improve NYKAA.</p>
          
          <h4>INFORMATION YOU GIVE US: </h4>
          <p className='para-font-size'>We receive and store any information you provide while using NYKAA, or give us in any other way. You can choose not to provide certain information, but then you might 
          not be able to use NYKAA. We use the information that you provide for such purposes as opening your account, processing your transactions, responding to your requests, 
          and communicating with you</p>
          
          <h4>INFORMATION WE COLLECT ABOUT YOU: </h4>
          <p className='para-font-size'>We receive and store any information you provide while using NYKAA, or give us in any other way. You can choose not to provide certain information, but then you might 
          not be able to use NYKAA. We use the information that you provide for such purposes as opening your account, processing your transactions, responding to your requests, 
          and communicating with you</p>
          
          <h4>CONSENT</h4>
          <p className='para-font-size'>By using the Website and/ or by providing your information, you consent to the collection and use of the information you disclose on the Website in accordance with 
          this Privacy Policy, including but not limited to your consent for sharing your information as per this privacy policy.</p>
          
          <h4>PURPOSES OF GATHERING YOUR PERSONAL DATA </h4>
          <p className='para-font-size'>We use your personal information to operate, provide, develop, and improve the products and services. These purposes include:</p>
          
          <h4>RECOMMENDATIONS AND PERSONALIZATION </h4>
          <p className='para-font-size'>We use your personal information to recommend features, products, and services that might be of interest to you, identify your preferences, and personalize your 
          experience with NYKAA.</p>

          <h4> DATA SECURITY </h4>
          <p className='para-font-size'>We are committed to protecting your Personal Data in our custody. We take reasonable steps to ensure appropriate physical, technical and managerial safeguards are 
          in place to protect your Personal Data from unauthorized access, alteration, transmission and deletion. We work to protect the security of your personal information 
          during transmission by using encryption protocols. We use multi-layered controls to help protect our infrastructure, constantly monitoring and improving our applications, 
          systems, and processes to meet the growing demands and challenges of security.We ensure that the third parties who provide services to us under appropriate contracts, 
          take appropriate security measures to protect your Personal Data in line with our policies.</p>

          <h4> YOUR RIGHTS AS A CUSTOMER </h4>
          <p className='para-font-size'>We understand that when you interact with Nykaa, you have rights over your Personal Data. These rights involve providing reasonable steps to allow you to access your 
          personal data, correct any errors among others. In the event that you are not satisfied with our response or have unresolved concerns, you can get in touch with us to 
          resolve the issue by means of privacy@nykaa.com.</p>

          <h4> CONTACT US </h4>
          <p className='para-font-size'>For any further queries and complaints related to privacy under applicable laws and regulations, you could reach us at:</p>
          <p className='para-font-size'>Contact Email Address: <a href="/">support@nykaa.com</a></p>

          <h4>NOTIFICATION OF CHANGES</h4>
          <p className='para-font-size'>We keep our Privacy Policy under regular review to make sure it is up to date and accurate. Any changes we may make to this Privacy Policy in the future will be 
          posted on this page. We recommend that you re-visit this page regularly to check for any updates.</p>
        </div>
      </div>
      <Footer />
    </div>
  )
}

export default PrivacyPolicyComponent
