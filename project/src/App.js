import React from "react"
// import { BrowserRouter } from "react-router-dom"
import Router from "./Routes/Router"

const App = () => {
  return (
    <Router />  
  )
}

export default App;
