import React from "react"
import { CardBody, CardImg } from "reactstrap"
import { X } from "react-feather"
import "./StyleSignUp.css"
import giftBox from "../img/Card/SignUpGiftBox.jpg"
import { Link } from "react-router-dom"

const SignUp = () => {
  return (
    <div className='grey-background'>
      <div className='app-user-view SignUp-card-width'>
        <div>
          <Link to='/'><X className='card-icon'/></Link>
          <CardBody className='card-content'>
            <p className='sign-up-heading'>Sign in</p>
            <p >To Quickly findout your favourites items, saved addresses and payments.</p>
            <hr />
            <p >Register and earn 2000 points</p>
            <CardImg className='img-size' src={giftBox}/>
            <button className='button'><Link className='SignUp-link' to='/ptype-auth'>Enter Phone Number or Email</Link></button>
            <button className='google-button'>Google</button>
            <p>By continuing, you agree that you have read and accept our <a className='anchor' href='/terms-conditions'>T&Cs </a> 
            and <a className='anchor' href='/privacy-policy'>Privacy Policy</a>.</p>
          </CardBody>
        </div>
      </div>
    </div>
  )
}

export default SignUp