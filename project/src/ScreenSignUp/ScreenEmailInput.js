import React from "react"
import { Card, Input, Form, FormGroup, Label } from "reactstrap"
import { X } from "react-feather"
import "./StyleSignUp.css"
import { Link } from "react-router-dom"
import "bootstrap/dist/css/bootstrap.css"

const ScreenEmailInput = () => {
  // const Email = useRef()
  return (
    <div className='grey-background'>
      <div className='app-user-view card-width'>
        <Card>
          <div className='header'>
            <Link to='/'><X className='card-icon'/></Link>
            <div className='card-icon'>LOGIN/REGISTER</div>
          </div>
          <Form className='form'>
            <FormGroup>
              <Label className='input-lable mt-3'>Email</Label>
              <Input type='text' name='email' className='input-field'/>
            </FormGroup>
            <FormGroup>
              <Label className='input-lable mt-2'>Password</Label>
              <Input type='password' name='password' className='input-field' minLength={8}/>
            </FormGroup>
            <FormGroup>
              <Label className='input-lable mt-2'>Confirm Password</Label>
              <Input type='password' name='confirm-password' className='input-field' />
            </FormGroup>
            <button type='submit' className='sign-up-submit'>Sign Up</button>
          </Form>
        </Card>
      </div>
    </div>  
  )
}

export default ScreenEmailInput