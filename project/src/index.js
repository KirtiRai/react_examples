import React from "react"
import ReactDOM from "react-dom"
import { BrowserRouter } from "react-router-dom"
import App from "./App"
import "./index.css"
import { LanguageContextPrivider } from "./LanguageContext"
import CartState from ".././src/Context/Cart/CartState"
import { store } from '../src/ScreenBrand/store/store'
import { Provider } from 'react-redux'

ReactDOM.render(
  <BrowserRouter>
    <LanguageContextPrivider>
      <React.StrictMode>
        <CartState>
        <Provider store={store}>
    <App />
  </Provider>,
        </CartState>
      </React.StrictMode>
    </LanguageContextPrivider>
  </BrowserRouter>,
  document.getElementById("root")
);
