import React from 'react'
import './StyleNavBarLower.css'

const NavBarLowerSubComponent = (props) => {
  return (
    <div className="lower-column">
      <h3>{props.category}</h3>
      {Array.isArray(props.content) && props.content.map((y) => {
        return (
          
          <a href="#">{y}</a>
        
        )
      })}
      {/* <a href="#">Link 2</a>*/}
    </div>
  )
}

export default NavBarLowerSubComponent