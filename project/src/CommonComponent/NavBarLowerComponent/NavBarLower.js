import React from "react"
import NavBarLowerSubComponent from "./NavBarLowerSubComponent"
import "./StyleNavBarLower.css"
import { LanguageHandler } from "../NavBarMiddleComponent/LanguageHandler";
import { useTranslation } from "../../Translation";
import { Home } from "react-feather";
import Sidebar from '../../ScreenBrand/Sidebar';
const NavBarLower = () => {
  const translation = useTranslation();
  return (
    <div className="lower-navbar">
      <div className="lower-dropdown">
        <button className="lower-dropbtn"> <span>{translation.Makeup}</span></button>
        <div className="lower-dropdown-content">
          <div>
            <NavBarLowerSubComponent category={"Face"} content={["Face Primer", "Concealer", "Foundation", "Compact", "Contour", "Loose Powder", "Blush", "Bronzer", "BB & CC creams", "Highlighters", "Setting Spary", "Makeup Remover"]}/>
          </div>
          <div className='lower-dropdown-content-color'>
            <NavBarLowerSubComponent category={"Eyes"} content={["Kajal", "Eye Liner", "Mascara", "Eye Shadow", "Eye Brow Enhancers", "Eye Primer", "False Eyelashes", "Eye Makeup Remover", "Under Eye Concealer", "Contact Lenses"]}/>
            <NavBarLowerSubComponent category={"Makeup Kits"} content={["Eye Palettes", "Face Palettes", "Customize Your Palette"]}/>
          </div>
          <div>
            <NavBarLowerSubComponent category={"Lips"} content={["Lipstick", "Liquid Lipstick", "Lip Crayon", "Lip Gloss", "Lip Liner", "Lip Plumper", "Lip Stain", "Lip Balm"]}/>
            <NavBarLowerSubComponent category={"Nails"} content={["Nail Polish", "Nail Art Kits", "Nail Polish Sets", "Nail Care", "Nail Polish Remover", "Manicure & Pedicure Kits"]}/>
          </div>
          <div className='lower-dropdown-content-color'>
            <NavBarLowerSubComponent category={"Tools & Brushes"} content={["Face Brush", "Blush Brush", "Face Brush", "Lip Brush", "Brushs Set", "Sponges & Applicators", "Eyelash Curlers", "Tweezers", "Sharpeners", "Mirrors", "Makeup Pouches"]}/>
          </div>
          <div>
            <NavBarLowerSubComponent category={"Top Brands"} content={["Kay Beauty", "Huda", "Charlotte Tilbury", "M.A.C", "Maybelline New York", "L'Oreal Paris", "Lakme", "Nykaa Cosmetics", "Nyk Pro.Makeup"]}/>
          </div>
          <div className='lower-dropdown-content-color'>
            <NavBarLowerSubComponent category={"Quick Links"} content={[
              "Combos@Nykaa", "New Launches", "NFBA Nominees 2020", 
              "Gifts @N ykaa", "The Gift Store"
            ]}/>
            <NavBarLowerSubComponent category={"Trending Searches"} content={["Nude Lipstick", "Matte Lipstick", "Red Lipstick", "Pink Lipstick"]}/>
          </div>
        </div>
      </div>
      <div className="lower-dropdown">
        <button className="lower-dropbtn"> <span>{translation.Skin}</span></button>
        <div className="lower-dropdown-content">
          <div>
            <NavBarLowerSubComponent category={"Cleansers"} content={["Facewash", "Cleansers", "Scrubs & Exfoliators", "Face Wipes", "Makeup Remover"]}/>
            <NavBarLowerSubComponent category={"Toners"} content={["Toners & Mists"]}/>
            <NavBarLowerSubComponent category={"Trending Searches"} content={["Toners Under 1000", "Face Wash for Oily Skin", "Oil Free Face", "Moisturizers", "Lip Balm Under 500", "Vitamin C Serum"]}/>
          </div>
          <div className='lower-dropdown-content-color'>
            <NavBarLowerSubComponent category={"Moisturizers"} content={["Face Moisturizer & Day Cream", "Night Cream", "Face Oils", "Serums & Essence", "BB & CC creams"]}/>
            <NavBarLowerSubComponent category={"Masks"} content={["Masks & Peels", "Sheet Masks"]}/>
            <NavBarLowerSubComponent category={"Kits & Combos"} content={["Facial Kits", "Combos @ Nykaa", "Gift Sets"]}/>
          </div>
          <div>
            <NavBarLowerSubComponent category={"Eye Care"} content={["Under Eye Cream & Serums", "Dark Circles & Wrinkles", "Puffiness", "Eye Masks", "Makeup Remover"]}/>
            <NavBarLowerSubComponent category={"Lip Care"} content={["Lip Balm", "Lip Scrub", "Lip Masks"]}/>
            <NavBarLowerSubComponent category={"Skin Supplements"} content={["Vitamins & Minerals", "Ayurvedic Herbs"]}/>
          </div>
          <div className='lower-dropdown-content-color'>
            <NavBarLowerSubComponent category={"Body Care"} content={["Body Butter", "Lotions & Creams", "Massage Oils", "Anti-Stretch Marks Creams", "Anti-Cellullite Creams", "Bath Salts", "Shower Gels & Body Wash", "Bath Scrubs", "Soaps"]}/>
            <NavBarLowerSubComponent category={"Sun Care"} content={["Face Sunscreen", "Body Sunscreen"]}/>
          </div>
          <div>
            <NavBarLowerSubComponent category={"Hands & Feet"} content={["Hand Creams", "Foot Creams", "Hand & Foot Masks"]}/>
            <NavBarLowerSubComponent category={"Top Brands"} content={["NYKAA SKINRX", "innisfree", "L'orel Paris", "Nykaa Naturals", "Neutrogena", "The Face Shop", "Nivea", "Mamaearth", "Plum"]}/>
          </div>
          <div className='lower-dropdown-content-color'>
            <NavBarLowerSubComponent category={"Shop By Concern"} content={["Skin Brightening", "Acne treatment", "Dull skin treatment", "Pigmentation", "Anti Aging", "Tan Removal", "Face Treatment", "Pore Care", "Sun Protection", "Skin Dryness", "Oil Control"]}/>
            <NavBarLowerSubComponent category={"New Launces"} content={[""]}/>
            <NavBarLowerSubComponent category={"Quick Links"} content={["The Gift Store"]}/>
          </div>
        </div>
      </div>
      <div className="lower-dropdown">
        <button className="lower-dropbtn"> <span>{translation.Hair}</span></button>
        <div className="lower-dropdown-content">
          <div>
            <NavBarLowerSubComponent category={"Hair Care"} content={["Nutitional Supplements", "Shampoo", "Dry Shampoo", "Conditioner", "Hair Oil", "Hair Serum", "Hair Cream & Masks"]}/>
          </div>
          <div className='lower-dropdown-content-color'>
            <NavBarLowerSubComponent category={"Tools & Accesories"} content={["Hair Brushes", "Hair Combs", "Dryers & Stylers", "Straightners", "Rollers & Curlers", "Hair Extensions", "Hair Accessories"]}/>           
          </div>
          <div>
            <NavBarLowerSubComponent category={"Hair Styling"} content={["Hair Colors", "Hair Sprays", "Gels & Waxes"]}/>           
            <NavBarLowerSubComponent category={"Shop By Hair Type"} content={["Straight", "Curly & Wavy"]}/>       
            <NavBarLowerSubComponent category={"Professional Brands"} content={[""]}/>    
          </div>
          <div className='lower-dropdown-content-color'>
            <NavBarLowerSubComponent category={"Shop By Concern"} content={["Hairfall & Thinning", "Dandruff", "Dry & Frizzy Hair", "Split Ends", "Color Protection"]}/>           
            <NavBarLowerSubComponent category={"Trending Searches"} content={["Hair Growth Oil", "Dandruff Shampoo", "Castor Oil for Hair", "Sulphate Free Shampoo", "Hair Straightner Brush"]}/>           
          </div>
          <div>
            <NavBarLowerSubComponent category={"Top Brands"} content={["Nykaa Naturals", "L'Oreal Paris", "Wella Professionals", "L'Oreal Professionals", "BBlunt", "Herbal Essence", "Schwarzkopf Professionals"]}/>           
          </div>
          <div className='lower-dropdown-content-color'>
            <NavBarLowerSubComponent category={"Quick Links"} content={["Combos @ Nykaa", "New Launches", "NFBA Nominees 20202", "Herbals Hair Care", "Routine Finder", "The Beauty Ingredient Edit", "The Safe Beauty Edit", "The Gift Store"]}/>           
          </div>
        </div>
      </div>
      <div className="lower-dropdown">
        <button className="lower-dropbtn"> <span>{translation.Appliances}</span></button>
        <div className="lower-dropdown-content">
          <div>
            <NavBarLowerSubComponent category={"Hair Styling Tools"} content={["Hair Dryers", "Straightner", "Curling Iron/Stylers", "Multi Stylers"]}/>
          </div>
          <div className='lower-dropdown-content-color'>
            <NavBarLowerSubComponent category={"Hair Removal Tools"} content={["Epilators", "Body Groomers"]}/>
          </div>
          <div>
            <NavBarLowerSubComponent category={"Shaving Tools"} content={["Shavers", "Trimmers"]}/>
          </div>
          <div className='lower-dropdown-content-color'>
            <NavBarLowerSubComponent category={"Face/Skin Tools"} content={["Face Epilators", "Dermarollers", "Cleansing Brushes", "Acne Removal"]}/>
            <NavBarLowerSubComponent category={"Massage Tools"} content={["Massagers"]}/>
            <NavBarLowerSubComponent category={"Foot Care"} content={[""]}/>
          </div>
          <div>
            <NavBarLowerSubComponent category={"Top Brands"} content={["Philips", "Alan Truman", "Dyson", "VEGA", "Braun", "Iconic Professionals", "Nova", "Flawless"]}/>
          </div>
          <div className='lower-dropdown-content-color'>
            <NavBarLowerSubComponent category={"Quick Links"} content={["Combos @ Nykaa", "New Launches", "NFBA Nominees 2020", "Gifts @ Nykaa", "Herbal Hair Care", "Routine Finder"]}/>
          </div>
        </div>
      </div>
      <div className="lower-dropdown">
        <button className="lower-dropbtn"> <span>{translation.PersonalCare}</span></button>
        <div className="lower-dropdown-content">
          <div>
            <NavBarLowerSubComponent category={"Bath & Shower"} content={["Shower Gels & Body Wash", "Scrubs & Exfoliants", "Soap", "Bath Salts"]}/>
            <NavBarLowerSubComponent category={"Body"} content={["Body Lotions & Creams", "Talcum Powder", "Deodorants/Roll-ons", ""]}/>
            <NavBarLowerSubComponent category={"Home & Salon"} content={[""]}/>
            <NavBarLowerSubComponent category={"Disposals"} content={[""]}/>
          </div>
          <div className='lower-dropdown-content-color'>
            <NavBarLowerSubComponent category={"Hands & Feet"} content={["Handwash", "Hand Sanitizers", "Hand Creams", "Foot Care", "Tissue Boxes & Handkerchiefs"]}/>
            <NavBarLowerSubComponent category={"Eye Care"} content={["Contact Lenses", "Lens Solution & Accessories"]}/>
          </div>
          <div>
            <NavBarLowerSubComponent category={"Feminine Hygiene"} content={["Sanitary Napkins", "Tampons", "Pantyliners", "Shaving & Hair Removal", "Intimate Hygiene", "Cotton Buds & Balls", "Menstrual Cups"]}/>
            <NavBarLowerSubComponent category={"Tools & Accessories"} content={["Manicure & Pedicure Kits", "Body Scrubbers & Brushes", "Loofahs & Sponges", "Shower Caps", "Miscellaneous"]}/>
          </div>
          <div className='lower-dropdown-content-color'>
            <NavBarLowerSubComponent category={"Men's Grooming"} content={["Shavers & Trimmers", "Razors & Catridges", "Shaving Cream, Foams & Gels", "Pre & Post Shaves", "Beard & Moustache Care", "Intimate Care"]}/>
            <NavBarLowerSubComponent category={"Dental Care"} content={["Toothpaste", "Manual Toothbrush", "Electric Toothbrush", "Floss & Tongue Cleaners", "Mouthwash", "Tooth Powder"]}/>
          </div>
          <div>
            <NavBarLowerSubComponent category={"Home & Health"} content={["Insect Repellants", "Pepper Spray", "Air Freshners", "Toilet Seat Sanitizer", "Face Mask", "Gloves", "Others"]}/>
            <NavBarLowerSubComponent category={"Travel Essentials"} content={[""]}/>
            <NavBarLowerSubComponent category={"Top Brands"} content={["Wanderlust", "Dove", "Gillete", "Pantene", "Palmolive", "Bella"]}/>
          </div>
          <div className='lower-dropdown-content-color'>
            <NavBarLowerSubComponent category={"Quick Links"} content={["Combos @ Nykaa", "New Launches", "NFBA Nominees 2020", "Gifts @ Nykaa", "Routine Finder", "The Gift Store"]}/>
            <NavBarLowerSubComponent category={"Trending Searches"} content={["Beard Oil", "Mosuito Repellant", "Menstrual Cup", "Hair Removal Cream", "Wax Strips"]}/>
          </div>
        </div>
      </div>
      <div className="lower-dropdown">
        <button className="lower-dropbtn"> <span>{translation.Natural}</span></button>
        <div className="lower-dropdown-content">
          <div>
            <NavBarLowerSubComponent category={"Skin"} content={["Face Wash", "Cleaner", "Moisturizer", "Face Cream", "Face Mist", "Facial Kits", "Toner", "Face Oils", "Sinscreen", "Night Cream", "Day Cream", "Under Eye Care", "Face Bleach", "Serums"]}/>
          </div>
          <div className='lower-dropdown-content-color'>
            <NavBarLowerSubComponent category={"Skin"} content={["Sheet Masks", "Masks & Peels", "Scrubs & Exfoliators", "Face Tools", "Face Gel", "Lip Scrub"]}/>
            <NavBarLowerSubComponent category={"Body Care"} content={["Shower Gels & Body Wash", "Soaps", "Body Lotions", "Body Scrubs", "Bath Salts & Bath Bombs", "Hands & Feet Care", "Bath Tools & Accessories", ""]}/>
          </div>
          <div>
            <NavBarLowerSubComponent category={"Hair"} content={["Shampoo & Cleanser", "Conditioner", "Hair Masks", "Hair Oil", "Hair Serum", "Hair Color", "Tools & Accessories"]}/>
            <NavBarLowerSubComponent category={"Aromatherapy"} content={["Massage Oils", "Carrier Oils", "Essential Olis", "Candles", "Diffuser", "Incense Sticks"]}/>
          </div>
          <div className='lower-dropdown-content-color'>
            <NavBarLowerSubComponent category={"Makeup"} content={["Lipstick", "Kajal", "Eyeliner", "Mascara", "Nail Polish", "Lip Balm & Gloss", "Foundation & Concealer", "Blush & Highlighter", "Makeup Remover", "Tools & Brushes"]}/>
            <NavBarLowerSubComponent category={"Trending Search"} content={["Tea Tree Oil", "Eucalyptus Oil", "Rosemary Oil", "Jojoba Oil", "Peppermint Oil"]}/>
          </div>
          <div>
            <NavBarLowerSubComponent category={"Top Brands"} content={["Biotique", "Lotus Herbals", "The Body Shop", "Nykaa Naturals", "Kama Ayurveda", "Forest Essentials", "Khadi Natural", "Himalaya", "VLCC"]}/>
            <NavBarLowerSubComponent category={"Baby Care"} content={[""]}/>
            <NavBarLowerSubComponent category={"Types of Skin"} content={["Dry Skin", "Normal Skin", "Oily Skin", "Combination Skin"]}/>
          </div>
          <div className='lower-dropdown-content-color'>
            <NavBarLowerSubComponent category={"Shop By Concern"} content={["Tan Removal", "Pigmentation", "Acne Treatment", "Skin Treatment", "Anti Aging", "Dark Circles", "Hairfall", "Dandruff", "Dry & Frizzy Hair"]}/>
            <NavBarLowerSubComponent category={"QUICK LINKS"} content={["New Launches", "Combos @ Nykaa", "Gifts @ Nykaa", "The Safe(and Clean)", "Beauty Edit"]}/>
          </div>
        </div>
      </div>
      <div className="lower-dropdown">
        <button className="lower-dropbtn"> <span>{translation.MomBaby}</span></button>
        <div className="lower-dropdown-content">
          <div>
            <NavBarLowerSubComponent category={"Baby Care"} content={["Body Wash & Soaps", "Baby Oil", "Hair Oil", "Lotions & Creams", "Baby Powder", "Shampoo & Conditioner", "Sunscreen", "Wipes & Buds", "Teeth & Dental Care", "Rash Cream", "Diapers", "Diapers Accessories", "Bath Accessories", "Baby Grooming", "Baby Bedding"]}/>
          </div>
          <div className='lower-dropdown-content-color'>
            <NavBarLowerSubComponent category={"Kids Care"} content={["Nutritional Supplement", "Body Wash & Soaps", "Lotions & Creams", "Haire Care", "Sunscreen", "Dental Care", "Kida Makeup"]}/>
          </div>
          <div>
            <NavBarLowerSubComponent category={"Maternity Care"} content={["Stretch Mark Creams & Oils", "Nutritional Supplements", "Maternity Pillows"]}/>
          </div>
          <div className='lower-dropdown-content-color'>
            <NavBarLowerSubComponent category={"Nursing & Feeding"} content={["Feeding Bottles", "Teethers", "Cleaning & Feeding Accessories", "Bibs", "Sippers"]}/>
          </div>
          <div>
            <NavBarLowerSubComponent category={"Health & Safety"} content={["Nose & Ear Care", "Gripe Water & Tummu Roll On", "Detergents & Cleansers", "Handwash & Sanitizer", "Mosquito Repellant"]}/>
            <NavBarLowerSubComponent category={"Maternity Wear"} content={["Maternity Dress", "Maternity Tops"]}/>
          </div>
          <div className='lower-dropdown-content-color'>
            <NavBarLowerSubComponent category={"Baby Toys"} content={[""]}/>
            <NavBarLowerSubComponent category={"Gift Sets"} content={[""]}/>
            <NavBarLowerSubComponent category={"Shop By Concerns"} content={["Baby Dry Skin", "Scalp Treatment", "Coconut Oil", "Almond Oil", "Heat Rash", "Body Toning & Firming", "Baby Skin Concerns"]}/>
            <NavBarLowerSubComponent category={"Combos @ Nykaa"} content={[""]}/>
          </div>
        </div>
      </div>
      <div className="lower-dropdown">
        <button className="lower-dropbtn"> <span>{translation.HealthWellness}</span></button>
        <div className="lower-dropdown-content">
          <div>
            <NavBarLowerSubComponent category={"Health Supplements"} content={["MultiVitamins", "Calcium & Vitamin D", "Magnesium & Zinc", "Omega 3 & Fish Oil", "Immunity Boosters & Vitamin C", "Other Supplements"]}/>
            <NavBarLowerSubComponent category={"Beauty Supplements"} content={["Collagen(Skin)", "Biotin(Hair)", "Vitamin E(Skin)", "Glutathione(Skin)", "Other Beauty", "Supplements"]}/>
          </div>
          <div className='lower-dropdown-content-color'>
            <NavBarLowerSubComponent category={"Sports Nutrition"} content={["Whey Protein", "Plant Protein", "BCAA & Other Muscle Support", "Protein & Energy Bars", "Protein Snacks"]}/>
            <NavBarLowerSubComponent category={"Weight Management"} content={["Weight Gain", "Apple Cider Vinegar", "Green Tea", "Green Coffee", "Fat Burner", "Slimming Shakes & Juices", "Sugar Substitutes"]}/>
          </div>
          <div>
            <NavBarLowerSubComponent category={"Health Foods"} content={["Honey", "Dry Fruits, Nuts & Berries", "Edible Seeds", "Oils & Ghee", "Black Tea & Coffee", "Herbals Teas", "Health Drinks", "Breakfast Cereals", "Other Health Foods"]}/>
            <NavBarLowerSubComponent category={"Wellness Equipment"} content={["Weighing Scales", "Fitness", "Face Steamers"]}/>
            <NavBarLowerSubComponent category={"Support & Braces"} content={[""]}/>
          </div>
          <div className='lower-dropdown-content-color'>
            <NavBarLowerSubComponent category={"Pain Relief"} content={["Muscles & Joints", "Ortho Oils", "Period Cramps", "Cough & Cold"]}/>
            <NavBarLowerSubComponent category={"Medical Devices"} content={["Oximeters"]}/>
          </div>
          <div>
            <NavBarLowerSubComponent category={"Ayurveda & Herbs"} content={["Ashwagandha", "Neem (BLood Purifier)", "Amla (Immunity, Skin)", "Aloe Vera (Liver, Skin)", "Milk Thistle (Liver)", "Wheatgrass(Detox)", "Tulsi", "Giloy & Guduchi (Immunity)", "Turmeric (Anti Inflamatory)", "Spirulina & Moringa", "Chyavaprash", "Shilajit", "Other Herbals", "Supplements"]}/>
          </div>
          <div className='lower-dropdown-content-color'>
            <NavBarLowerSubComponent category={"Shop By Concern"} content={["Diabetes", "Digestion (Gut Health)", "Organs - Liver,  Heart, Kidney", "Safety & First Aid", "Weakness & Vitality", "Kida Nutrition", "Mental Wellness", "Blood Pressure", "Hormonal Balance", "Calm & Sleep"]}/>
          </div>
        </div>
      </div>
      <div className="lower-dropdown">
        <button className="lower-dropbtn"> <span>{translation.Men}</span></button>
        <div className="lower-dropdown-content">
          <div>
            <NavBarLowerSubComponent category={"Shaving"} content={["Razors & Cartridges", "Shavers", "Trimmers", "Shaving Creams", "Shaving Foams", "Shaving Gels", "Pre & Post Shaves", "Aftershave Lotion", "Shaving Brushes"]}/>
          </div>
          <div className='lower-dropdown-content-color'>
            <NavBarLowerSubComponent category={"Beard Care"} content={["Beard Oil", "Beard Butter", "Beard Softener", "Beard Wash", "Beard Wax", "Moustache Oil", "Beard Comb", "Moustache Wax", "Beard Kits", "Beard Gel", "Beard Balm", "Beard Cream", "Beard Serum", "Beard Mist", "Beard Color", "Beard Shampoo"]}/>
          </div>
          <div>
            <NavBarLowerSubComponent category={"Hair Care"} content={["Shampoo", "Conditioner", "Hair Styling", "Hair Color", "Hair Oils", "Professional Products"]}/>
            <NavBarLowerSubComponent category={"Skin Care"} content={["Face Wash", "Moisturizers", "Sunscreen", "Masks & Peels", "Scrubs & Exfoliators", "Fairness"]}/>
          </div>
          <div className='lower-dropdown-content-color'>
            <NavBarLowerSubComponent category={"Bath & Body"} content={["Bath/Shower Gels", "Soaps", "Body Scrubs", "Talc", "Dental Care", "Body Lotions", "Intimate Care"]}/>
            <NavBarLowerSubComponent category={"Grooming Kits"} content={[""]}/>
            <NavBarLowerSubComponent category={"Fragrance"} content={["Deodorants/Roll Ons", "Colognes & Perfumes (EDT & EDP)", "Luxe Fragrances"]}/>
          </div>
          <div>
            <NavBarLowerSubComponent category={"Shop By Concern"} content={["Anti Dandruff", "Anti Hairfall", "Scalp Treatment", "Anti Acne", "Anti Ageing"]}/>
            <NavBarLowerSubComponent category={"Wellness"} content={["Health Supplements", "Weight Management", "Sports Nutrition"]}/>
          </div>
          <div className='lower-dropdown-content-color'>
            <NavBarLowerSubComponent category={"Top Brands"} content={["Beardo", "Gilette", "Livon", "Nivea", "Park Avenue"]}/>
            <NavBarLowerSubComponent category={"Quick Links"} content={["Combos @ Nykaa", "New Launches", "Gifts @ Nykaa", "Routine Finder", "The Gift Store"]}/>
          </div>
        </div>
      </div>
      <div className="lower-dropdown">
        <button className="lower-dropbtn"> <span>{translation.Fragrance}</span></button>
        <div className="lower-dropdown-content">
          <div>
            <NavBarLowerSubComponent category={"Women's Fragrance"} content={["Perfumes (EDT/EDP)", "Body Mists/Sprays", "Deodorants/Roll-ons"]}/>
            <NavBarLowerSubComponent category={"Men's Fragrance"} content={["Perfumes (EDT/EDP)", "Body Mists/Sprays", "Deodorants/Roll-ons", "Colognes & After Shaves"]}/>
          </div>
          <div className='lower-dropdown-content-color'>
            <NavBarLowerSubComponent category={"Giftsets & Combos"} content={[""]}/>
            <NavBarLowerSubComponent category={"Shop By Fragrance Family"} content={["Earthy & Woody", "Floral", "Fresh & Aquatic", "Spicy & Warm", "Oud Collection", "Fruity"]}/>
            <NavBarLowerSubComponent category={"The Parcos Store"} content={[""]}/>
          </div>
          <div>
            <NavBarLowerSubComponent category={"Ultra Luxury"} content={[""]}/>
            <NavBarLowerSubComponent category={"Candles"} content={[""]}/>
          </div>
          <div className='lower-dropdown-content-color'>
            <NavBarLowerSubComponent category={"Top Brands"} content={["Nykaa Cosmetics", "Masaba By Nykaa", "Dior", "Gucci", "Calvin Klein", "Davidoff", "Hermes", "Bvlgari", "Versace", "Skinn By Titan", "Paco Rabanne", "Giorgio Armani"]}/>
          </div>
          <div>
            <NavBarLowerSubComponent category={"PREMIUM AND DESIGNER BRANDS"} content={["Explore All", "Dior", "Hermes", "Jo Malone London", "Guerlain", "BVLGARI", "Salvatore Ferragamo", "Calvin Klein", "Giorgio Armani", "Davidoff", "Paco Rabanne", "Carolina Herrera", "Yves Saint Laurant", "Elie Saab"]}/>
          </div>
          <div className='lower-dropdown-content-color'>
            <NavBarLowerSubComponent category={""} content={["Doce & Gabana", "Narciso Rodrigue", "Hugo Boss", "Montblanc"]}/>
            <NavBarLowerSubComponent category={"Quick Links"} content={["Combos @ Nykaa", "New Launches", "NFBA Nominees 2020", "Help Me Choose - Fragrance Finder", "Gifts @ Nykaa", "The Gift Store"]}/>
          </div>
        </div>
      </div>
      <div className="lower-dropdown">
        <button className="lower-dropbtn"> <span>{translation.Luxe}</span></button>
        <div className="lower-dropdown-content">
          <div>
            <NavBarLowerSubComponent category={"Face"} content={["Primer", "Corrector & Concealer", "Foundation", "Compact", "Loose Powder", "Blush", "Bronzer", "Highlighter", "Tinted Moisturizer", "BB & CC Cream", "Makeup Remover", "Contouring", "Customize Your Pallete", "Setting Spray"]}/>
          </div>
          <div>
            <NavBarLowerSubComponent category={"Eyes"} content={["Eyliner & Kajal", "Mascara", "Eye Shadow", "Brows", "Lashes", "Eye Primer", "Corrector & Concealer", "Customize Your Pallete"]}/>
            <NavBarLowerSubComponent category={"Lips"} content={["Lipstick", "Liquid Lipstick", "Lip Gloss", "Lip Liner", "Lip Balm"]}/>
          </div>
          <div>
            <NavBarLowerSubComponent category={"Nails"} content={["Nail Polish", "Nail Care", "Nail Polish Remover"]}/>
            <NavBarLowerSubComponent category={"Palettes"} content={["Eye Palette", "Face Palette", "Customize Your Palette"]}/>
            <NavBarLowerSubComponent category={"Tools & Brushes"} content={["Face Brush", "Eye Brush", "Lip Brush", "Applicators"]}/>
          </div>
          <div className='lower-dropdown-content-color'>
            <NavBarLowerSubComponent category={"Face"} content={["Face Wash & Cleansers", "Toner", "Exfoliators", "Serum", "Moisturizers", "Day Cream", "Night Cream", "Masks", "Sunscreen", "Face Mist", "Makeup Remover", "Tools & Appliances"]}/>
            <NavBarLowerSubComponent category={"Lipcare"} content={[""]}/>
          </div>
          <div className='lower-dropdown-content-color'>
            <NavBarLowerSubComponent category={"Eyecare"} content={["Eye Cream", "Serum", "Dark Circles"]}/>
            <NavBarLowerSubComponent category={"Shop By Concern"} content={["Anti-Blemish", "Brightening", "Pigmentation", "Oil Control", "Dryness", "Anti-Ageing", "Perfumes(EDP/EDT)", "Deodorants", "Colognes"]}/>
          </div>
          <div>
            <NavBarLowerSubComponent category={""} content={["Perfumes(EDP/EDT)", "Aftershave", "Face & Body Mists", "Candles & Incense", "Gifts"]}/>
            <NavBarLowerSubComponent category={"Hair"} content={["Shampoo", "Conditioner", "Hair Oil", "Hair Serums & Masks", "Hair Styling", "Hair Color", "Appliances"]}/>
          </div>
          <div className='lower-dropdown-content-color'>
            <NavBarLowerSubComponent category={"Bath"} content={["Soap", "Shower Gel", "Scrubs & Exfoliators"]}/>
            <NavBarLowerSubComponent category={"Body"} content={["Moisturizers", "Sun Protection"]}/>
            <NavBarLowerSubComponent category={"Gifts"} content={[""]}/>
          </div>
        </div>
      </div>
    </div> 
    
  )
}

export default NavBarLower
