
import React from "react"
import "./StyleFooter.css"
import { Mail, Smartphone, Phone } from "react-feather"
import AppStoreIcon from "../../img/FooterImg/AppStoresIcon.jpg"
import BusIcon from "../../img/FooterImg/BusIcon.jpg"
import CheckIcon from "../../img/FooterImg/checkIcon.jpg"
import PriceTagIcon from "../../img/FooterImg/PriceTagIcon.jpg"
import RewardIcon from "../../img/FooterImg/RewardIcon.jpg"
import SocialMediaIcon from "../../img/FooterImg/SocialMediaIcon.jpg"
import WhiteNykaaIcon from "../../img/FooterImg/WhiteNykaaIcon.jpg"
const Footer = () => {
  return (
    <div>
      <div className='container-one'>
        <div className='flex-item'>
          <div><Mail style={{marginRight: "5%"}} size='30px'/><span style={{fontSize: "0.85rem"}}>GET SPECIAL DISCOUNT IN YOUR INBOX</span></div>
          <div>
            <input type='text' placeholder='Enter Your Email Id' className='input'/>
            <button className='footer-button'>SEND</button>
          </div>
        </div>
        <div className='flex-item'>
          <div>
            <Smartphone style={{marginRight: "5%"}} />EXPERIENCE THE NYKAA APP
            <img src={AppStoreIcon}/>
          </div>
        </div>
        <div className='flex-item' style={{marginTop: "2%"}}>
          <Phone style={{marginRight: "5%"}} />FOR ANY HELP YOU MAY CALL US AT<br/>1800-267-4444<br />
          (Monday to Saturday, 8AM to 10PM and Sunday, 10AM to 7PM)
        </div>
      </div>
      <div className='container-two'>
        <div className='footer-row'>
          <div className='flex-item-two'>
            <li><img width='90px' height='50px' src={WhiteNykaaIcon} /></li>
            <li>WHO ARE WE?</li>
            <li>CAREERS</li> 
            <li>AUTHENTICITY</li> 
            <li>PRESS</li> 
            <li>TESTIMONIALS</li>
            <li>NYKAA CSR</li> 
            <li>RESPONSIBLE DISCLOSURE</li>
            <li>INVESTOR RELATIONS</li>
          </div>

          <div className='flex-item-two'>
            <li>HELP</li>
            <li>CONTACT US</li>
            <li>FREQUENTLY ASKED QUESTIONS</li> 
            <li>STORE LOCATOR</li> 
            <li>CANCELLATION & RETURN</li> 
            <li>SHIPPING & DELIVERY</li>
            <li>SELL ON NYKAA</li>
          </div>

          <div className='flex-item-two'>
            <li>INSPIRE ME</li>
            <li>BEAUTY BOOK</li>
            <li>NYKAA TV</li> 
            <li>NYKAA NETWORK</li> 
            <li>BUYING GUIDES</li> 
          </div>

          <div className='flex-item-two'>
            <li>QUICK LINKS</li>
            <li>OFFER ZONE</li>
            <li>NEW LAUNCHES</li> 
            <li>NYKSS MAN</li> 
            <li>NYKAA FASHION</li> 
            <li>NYKAA PRO</li>
            <li>NYKAA CSR</li> 
            <li>NYKAA FAMINA BEAUTY AWARDS WINNERS 2019</li>
            <li>SITEMAP</li>
          </div>

          <div className='flex-item-two'>
            <li>TOP CATEGORIES</li>
            <li>MAKEUP</li>
            <li>SKIN</li> 
            <li>HAIR</li> 
            <li>PERSONAL CARE</li> 
            <li>APPLIANCES</li>
          </div>
          <div className='flex-item-two'>
            <li><br /></li>
            <li>MOM AND BABY</li>
            <li>WELLNESS</li> 
            <li>FRAGRANCE</li> 
            <li>NATURAL</li> 
            <li>LUXE</li>
          </div>
        </div>
      </div>
      <div className='container-three'>
        <div className='flex-item-three'>
          <img className='img-container' src={BusIcon}/>
          <div>FREE SHIPPING<br /><hr />for orders above INR 299</div>
        </div>
        <div className='flex-item-three'>
          <img className='img-container' src={CheckIcon}/>
          <div>EASY RETURNS<br /><hr />15 Day Easy Returns For Most Products</div>
        </div>
        <div className='flex-item-three'>
          <img className='img-container' src={RewardIcon}/>
          <div>AUTHENTIC PRODUCTS<br /><hr />Sourced Directory Brands/Authorised Distributers</div>
        </div>
        <div className='flex-item-three'>
          <img className='img-container' src={PriceTagIcon}/>
          <div>2400+ BRANDS<br /><hr />Well Curated 1.9Lakh + Product</div>
        </div>
        <div className='flex-item-three'>
          <div>SHOW US SOME LOVE ON SOCIAL MEDIA</div>
          <img src={SocialMediaIcon}/>
        </div>
      </div>
      <div className='container-four'>
        <div>
          <span style={{margin: "0% 3%"}}>TERMS & CONDITIONS</span>
          <span style={{margin: "0% 3%"}}>SHIPPING POLICY</span>
          <span style={{margin: "0% 3%"}}>CANCELLATION POLICY</span>
          <span style={{margin: "0% 3%"}}>PRIVACY POLICY</span>
        </div>
        <div>2022 Nykaa E-Retail Pvt Ltd. All Rights Reserved.</div>
      </div>
    </div>
  )
}

export default Footer