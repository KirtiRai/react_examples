import React from "react";
import { useLanguageContext } from "../../../src/LanguageContext"

export const LanguageHandler = ()=> {
  const { language, changeLanguage } = useLanguageContext();

  return (
    <select value={language} onChange={(e) => changeLanguage(e.target.value)}>
      <option value="en">En- English</option>
      <option value="fr">Fr- France</option>
    </select>
  );
}