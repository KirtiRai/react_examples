import React from "react";
import { Button } from "reactstrap";
import "./StyleNavBarMiddle.css"
import pic1 from "../../img/NavBarMiddleImg/pic1.png"
import pic3 from "../../img/NavBarMiddleImg/pic4.png"
import pic4 from "../../img/NavBarMiddleImg/pic5.png"
import pic5 from "../../img/NavBarMiddleImg/pic7.png"

const Brands = () => {
  return (
    <div>
      <div className="middle-row">
        <div className="middle-column">
          <input className="form-control mr-sm-2 _search" type="search" placeholder="Search Brands" aria-label="Search" />
        </div>
        <div className="middle-column">
          <div className="middle-row">
            <button className="_btn_">Popular</button>
            <button className="_btn_">Featured</button>
            <button className="_btn_">Only at Nykaa</button>
            <button className="_btn_">New Launches</button>
          </div>
          <div className="middle-row">
            <img className="_img_" src={pic1} alt="pic"/>
            <img className="_img_" src={pic3} alt="pic" />
            <img className="_img_" src={pic5} alt="pic"/>
            <img className="_img_" src={pic4} alt="pic" />
          </div>
        </div>
      </div>
    </div>
  )
}

export default Brands