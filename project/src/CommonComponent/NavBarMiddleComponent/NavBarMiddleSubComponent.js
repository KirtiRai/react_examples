/* eslint-disable react/jsx-key */
import React from "react"
import "./StyleNavBarMiddle.css"

const MiddleSubComponent=(props) => {
  return (
    <div className="middle-column">
      <h3>{props.category}</h3>
       
      {/* <a href="#">{x}</a> */}
     
      {Array.isArray(props.content) && props.content.map((x) => {
        return (
          <a href="#">{x}</a>
        )
      })}
    </div>

  )
}
export default MiddleSubComponent