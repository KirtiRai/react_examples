import React from "react"
import middle1 from "../../img/NavBarMiddleImg/middle.jpg"
import middle2 from "../../img/NavBarMiddleImg/middle2.png"
import middle3 from "../../img/NavBarMiddleImg/middle3.jpg"
import "./StyleNavBarMiddle.css"

const MiddleComponent = () => {
  return (
    <div className="advice-container">
      <div className="advice-section"><img width='300px' src={middle1} alt="qr" /></div>
      <div className="advice-section"><img width='300px' src={middle2} alt="qwr" /></div>
      <div className="advice-section"><img width='300px' src={middle3} alt="qwr" /></div>
    </div> 
  )  
}

export default MiddleComponent
