
import React, {useContext} from "react"
import { User } from "react-feather"
import "font-awesome/css/font-awesome.min.css"
import Nykaa from "../../img/NavBarMiddleImg/logo.png"
import "./StyleNavBarMiddle.css"
// import '../NavBarLowerComponent/StyleNavBarLower.css'
import MiddleSubComponent from "./NavBarMiddleSubComponent"
import MiddleComponent from "./MiddleComponent"
import Brands from "./Brands"
import { LanguageHandler } from "./LanguageHandler"
import { useTranslation } from "../../Translation"
import { Link } from "react-router-dom"
import CartContext from "../../Context/Cart/CartContext"
// import ScreenHome from "../../ScreenHome/ScreenHome"
const NavBarMiddle = () => {
  const {cartItems, showHideCart} = useContext(CartContext)
  const translation = useTranslation()
  return (
    <div>
      <div className="middle-navbar">
        <div className="middle-dropdown" >
          <img className="_logo" src={Nykaa} />
        </div>

        <div className="middle-dropdown">
          <button className="middle-dropbtn"> <span className="_black">{translation.Categories}</span></button>
        </div>

        <div className="middle-dropdown">
          <button className="middle-dropbtn"><span className="_black">{translation.Brands}</span></button>
          <div className="middle-dropdown-content">
            <div>
              <Brands />
            </div>
          </div>
        </div>

        <div className="middle-dropdown">
          <button className="middle-dropbtn"><span className="_black">{translation.NykaFashion}</span></button>
          <div className="middle-dropdown-content">
            <div className="middle-row">
              <div className="middle-column">
                <h3>Lingerie & More</h3>
                <MiddleSubComponent category={"Top Brands"} content={["NYKD By Nykaa", "Enamor", "Jockey", "Triumph", "Puma", "Clovia", "Zivame", "Wacoal", "Amante", "Dermawear", "S.O.I.E", "Nite-Flite", "Da Intimo"]} />
              </div>
            </div>

            <div className="middle-row">
              <div className="middle-column">
                <h3>Lingerie & More</h3>
                <div className="middle-column">
                  {/* < MiddleSubComponent category={'Top Brands'} content={['NYKD By Nykaa', 'Enamor', 'Jockey', 'Triumph', 'Puma', 'Clovia', 'Zivame', 'Wacoal', 'Amante', 'Dermawear', 'S.O.I.E', 'Nite-Flite', 'Da Intimo']}/> */}
                </div>
                <h3><span className="_black">{translation.NykaaFashion}</span></h3>
                <MiddleSubComponent category={""} content={["T-Shirt Bra", "Bralette", "Push-Up Bra", "Strapless Bra", "Padded Bra", "Minimizer Bra"]} />
              </div>
              <div className="middle-column">
                <h3>Clothing & More</h3>
                <MiddleSubComponent category={"Underwear"} content={["Borshts", "Thong", "Shortless"]} />
              </div>
              <div className="middle-column">
                <h3>Bags & Footwear</h3>
                <MiddleSubComponent category={"Underwear"} content={["Boyshots", "Thong", "Shortless"]} />
              </div>
              <div className="middle-column">
                <h3>Jewellery & Accessories</h3>
                <MiddleSubComponent category={"FITNESS ACCESSORIES"} content={[""]} />
              </div>
              <div className="middle-column">
                <h3>Gadgets & Tech Accessories</h3>
                <MiddleSubComponent category={"FACE MASKS"} content={[""]} />
              </div>
            </div>
          </div>
        </div>

        <div className="middle-dropdown">
          <button className="middle-dropbtn"><span className="_black">{translation.BeautyAdvice}</span></button>
          <div className="middle-dropdown-content">
            <div><MiddleComponent /></div>
          </div>
        </div>

        <div className="middle-dropdown">
          <button className="middle-dropbtn"><span className="_black">{translation.NykaaNetwork}</span></button>
        </div>

        <div className="middle-dropdown">
          <div className="search-container">
            <button type="submit"><i className="fa fa-search"></i></button>
            <input type="text" placeholder={translation.SearchNykaa} />
          </div>
        </div>

        <div className="middle-dropdown icon">
          <span className="_black"><User size={25} /><button className="myAccount"><Link className="middle-link" to='/auth'>{translation.Account}</Link></button></span>
        </div>

        {/* <div className="middle-dropdown dropdown _Bag" > */}
        
        <div className="cart_icon">
         <i className="fa fa-shopping-cart" area-hidden="true" onClick={showHideCart} />
          {cartItems.length > 0 && <div className="item_count"><span className="_cart_Items">{cartItems.length}</span></div>}  
        </div>
       
        {/* <ShoppingBag size={30} /> */}
        {/* </div> */}

        <div className="middle-dropdown dropdown_Btn">
          <span className="_black">{translation.changeLanguage}</span>
          <LanguageHandler />
        </div>
      </div>
    </div>
  )
}

export default NavBarMiddle
