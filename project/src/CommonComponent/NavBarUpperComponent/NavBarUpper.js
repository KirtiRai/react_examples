import React from "react"
import {Smartphone, MapPin, Gift, HelpCircle} from "react-feather"
import "./NavBarUpper.css"
import { Anchor } from "antd"
import {useTranslation} from "../../Translation"
import { LanguageHandler } from "../NavBarMiddleComponent/LanguageHandler";
const {Link} = Anchor
const NavBarUpper = () => { 
  const translation = useTranslation(); 
  return (
    <div className="_UpperNav">
      <div id="offer-banner" className="home-top-menu-fixed top-strip-bg">
        <div className="top_strip_wrapper">
          <div className="leftDiv ts-wrap-l">
            <div className="left-message">     
              <a href="" target="_blank" rel="reopener noreference"><span className="ts-text _black">{translation.Beauty}</span> &nbsp;::after</a>
            </div>
          </div>
          <div className="rightDiv ts-wrap-r">
            <ul>
              <li >
                <a href="/getApp" target=" "rel="noopener noreferrer">
                  <span className="ts-icon"></span>
                  <span><Smartphone  size={20}/></span>
                  <span  className="ts-text _black">{translation.getApp}</span>              
                </a>             
              </li>
              <li >
                <a href="#store" target="_blank" rel="noopener noreferrer">
                  <span className="ts-icon"></span>
                  <span><MapPin size={20}/></span>
                  <span className="ts-text _black">{translation.store}</span>
                </a>
              </li>

              <li >
                <a href="#gift" target="_blank" rel="noopener noreferrer">
                  <span className="ts-icon"></span>
                  <span><Gift  size={20}/></span>
                  <span className="ts-text _black">{translation.giftCard}</span>
                
                </a>
              </li>
                        
              <li>
                <a href="#help" target="_blank" rel="noopener noreferrer">
                  <span className="ts-icon "></span>
                  <span ><HelpCircle  size={20}/> </span>
                  <span className="ts-text _black">{translation.help}</span>
                  
                </a>
              </li>
            </ul>

          </div>
        </div>

      </div>
    </div>
    
  )

}
export default NavBarUpper
     