import React from "react";
import Box from '@mui/material/Box';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import InboxIcon from '@mui/icons-material/MoveToInbox';
import MailIcon from '@mui/icons-material/Mail';
const List =()=>{
    const [state, setState] = React.useState({
        left: false,
        
      });
      const toggleDrawer = (anchor, open) => (event) => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
          return;
        }
    
        setState({ ...state, [anchor]: open });
      };
    return(
        <Box
        sx={{ width: anchor === 'top' || anchor === 'bottom' ? 'auto' : 250 }}
        role="presentation"
        onClick={toggleDrawer(anchor, false)}
        onKeyDown={toggleDrawer(anchor, false)}
      >
          <List>
              {['Sort By: Popularity'].map((text,index) => ( 
                  <ListItem button key={text}>
              <ListItemText primary={text} />
            </ListItem>
              ))}
          </List>
          <Divider />
        <List>
          {['Categories', 'Price', 'Discount', 'Concern ', 'Age', 'Gender', 'Skin-type', 'Formulation', 'Preference','Pack-Size', 'Average Customer Rating', 'SPF'].map((text,index)=>(
          <ListItem button key={text}>
              <ListItemText primary={text} />
            </ListItem>
          ))}
          
        </List>
       
      </Box>
    )

}
export default List