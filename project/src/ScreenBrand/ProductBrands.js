
import React,{ useEffect, useRef }  from "react"
import NavBarLower from "../CommonComponent/NavBarLowerComponent/NavBarLower"
import NavBarMiddle from "../CommonComponent/NavBarMiddleComponent/NavBarMiddle"
import NavBarUpper from "../CommonComponent/NavBarUpperComponent/NavBarUpper"
import Products from "./data"
import Footer from "../CommonComponent/FooterComponent/Footer"
import ScreenBrandProduct from "./ScreenBrandProduct"
import Cart from "../ScreenBrand/Cart"
import Sidebar from "./Sidebar"
import './sidebar.css'
import { useSelector } from "react-redux"
const ProductBrands=() => {
  
  const topContainer = useRef();

  useEffect(() => {
    topContainer.current.scrollIntoView({ block: "end", behavior: 'smooth' });
    }, []);
  
  const ProductData = useSelector((state) => state.counter.allProducts)
  
  return (
    <div>
      <div ref={topContainer} />
      <NavBarUpper />
      <NavBarMiddle />
      <NavBarLower />
      <Cart />
      <Sidebar  />
      <h1 className="text-center mt-2 "> All Products</h1>
      <section className="py-4 container">
        <div className="row justify-content-center">
        
        {
          
           ProductData.map((product) => {
             return (
               <ScreenBrandProduct key={product.id} product={product}/>
             ) 
            
           })
         }
         
        </div>
      </section>
      <Footer />
    </div>
    
  )
}
export default ProductBrands