import React, { useContext } from "react";
import "../../src/CommonComponent/NavBarMiddleComponent/StyleNavBarMiddle.css"
import "../../src/ScreenBrand/ScreenBrand.css"
import { ArrowLeft, ArrowRight } from "react-feather";
import CartContext from "../../src/Context/Cart/CartContext";
import CartItem from "../ScreenBrand/CartItem";
import SampleImg from "../img/Card/SignUpGiftBox.jpg"
import {useNavigate} from "react-router-dom"
const Cart = () => {
  const {showCart, cartItems, showHideCart} = useContext(CartContext)
  const navigate = useNavigate();
  const clickHandler =()=>{
   navigate('/')

  }
  return (
      <div>
          {showCart && (
              <div className="cart_Wrapper_">
              <div style={{textAlign:"right"}}>
              <i style={{cursor:"pointer"}}
              className="fa fa-time-circle"
              aria-hidden="true"
              onClick={showHideCart}>
              </i>
              </div>
              <div className="cart_innerWrapper">
                  {cartItems.length===0 ? (
                      <div>
                          <div className="lower-navbar">
                          
                              <ArrowLeft style={{color:"rgb(233, 15, 117)"}} onClick={showHideCart}/>
                              
                              <h3 style={{marginLeft:130}}>Shopping Bag</h3>
                          </div>
                               <img style={{marginTop:40}} src={SampleImg} alt="Card image cap" />

                          <h4 style={{textAlign:'center' , marginTop:40}}>Your Shopping Bag is empty</h4>
                          <button onClick={showHideCart} className="btn btn-danger" style={{display:"flex", justifyContent:"center", placeItems:"center", marginLeft:150 , marginTop:40}}>Start Shopping <ArrowRight/></button>
                          </div>
                  ) : (
                      <ul>
                          {cartItems.map(item=>(
                              <CartItem key={item.id} item={item} />


                          ))}
                      </ul>
                  )}

              </div>
              <div className="cart_cartTotal">
                  <div> cart total</div>
                  <div></div>
                  <div style={{marginLeft:5}}>
                  {cartItems.reduce((amount,item)=> item.price + amount,0)}
                  </div>
              </div>
              </div>
          )}

      </div>

  )}

export default Cart