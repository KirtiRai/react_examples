/* eslint-disable react/jsx-key */
import "./ScreenBrand.css"
import React, {useContext} from "react"
import CartContext from "../Context/Cart/CartContext"
import { Link } from "react-router-dom"
import {Heart} from "react-feather" 
const ScreenBrandProduct = ({product}) => {
  const { addToCart } = useContext(CartContext)
  return (
    <div className=' col-md-3  mx-0 mb-4 '>
        <div className="card p-0 overflow-hidden h-300 shadow">
        <img className="card-img-top img-fluid" src={product.img} alt={"img"}/>
        <h5 className="card-title_">{product.title}</h5>
          <h5 className="card-title_">{product.desc}</h5>
          <p className="card-text_"> {product.price} Rs.</p>
        <div className="card-body">
          <div className="heartBtn">
           <button className="_heart"><Link to ='/auth'className="_heart">  <Heart /> </Link></button>
            <button className="btn btn-danger button_ "  onClick={() => addToCart(product) }>Add To Cart</button>
          </div>
        </div>
      </div>
    </div>
 
  )
}
export default ScreenBrandProduct
