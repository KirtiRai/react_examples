import product1 from ".././img/ProductImg/product1.jpg"
import product2 from ".././img/ProductImg/product2.jpg"
import product3 from ".././img/ProductImg/product3.jpg"
const Products = [
  {
    id:1,
    title:"Liquid Lipstick",
    img:product1,
    desc:"this is good",
    price:456,
    type:"lipstick"
  },
  {
    id:2,
    title:"face-powder",
    img:product2,
    desc:"this is good",
    price:200,
    type:"face-powder"
  },
  {
    id:3,
    title:"foundation",
    img:product3,
    desc:"this is good",
    price:300,
    type:"foundation"
  },
  {
    id:4,
    title:"Liquid Lipstick",
    img:product1,
    desc:"this is good",
    price:430,
    type:"lipstick"
  },
  {
    id:5,
    title:"face-powder",
    img:product2,
    desc:"this is good",
    price:275,
    type:"face-powder"
  },
  {
    id:6,
    title:"foundation",
    img:product3,
    desc:"this is good",
    price:600,
    type:"foundation"
  },
  {
    id:7,
    title:"foundation",
    img:product3,
    desc:"this is good",
    price:700,
    type:"foundation"
  },
  {
    id:8,
    title:"face-powder",
    img:product2,
    desc:"this is good",
    price:400,
    type:"face-powder"
  },

]

export default Products