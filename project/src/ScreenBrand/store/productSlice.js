import { createSlice } from '@reduxjs/toolkit'
import Products from '../data'
const initialState = {
  allProducts:Products,
};

export const productSlice = createSlice({
  name: 'counter',
  initialState,
  reducers: {
    productList: (state, action) => {
      const {type} = action.payload;
      console.log(Products, 'moni');
      
     const allProduct = Products.filter((product) =>product.type === type);
      state.allProducts=allProduct
    
     },
  },
})

// Action creators are generated for each case reducer function
export const { productList } = productSlice.actions

export default productSlice.reducer