import React, { useContext } from "react";
import CartContext from "../../src/Context/Cart/CartContext";
import "../../src/CommonComponent/NavBarMiddleComponent/StyleNavBarMiddle.css"
const CartItem = ({item}) =>{
    const {removeToCart} = useContext(CartContext)
    return(
            <li className="CartItem_item">
                 <img src={item.img} alt="img" />
                 <div className="card-body">
          <h5 className="card-title">{item.title}</h5>
          <h5 className="card-title">{item.desc}</h5>
          <p className="card-text">$ {item.price} Rs.</p>
          <div className="_heart_btn">
        <button className="btn btn-danger" onClick={()=>removeToCart(item.id)}>Remove Product</button>
          </div>
        </div>
            </li>
    )
}
export default CartItem