import React from 'react';
import { slide as Menu } from 'react-burger-menu';
import './sidebar.css'
import { useSelector, useDispatch } from 'react-redux'
import { productList } from './store/productSlice'

export default props => {
  
  const dispatch = useDispatch()

  return (
    <Menu>
      <a className="menu-item" href="/">
       Categories
      </a>
      <a className="menu-item" onClick={() => dispatch(productList({ type: "foundation" }))} href="/">
        Foundation 
      </a>
      <a className="menu-item" onClick={() => dispatch(productList({ type: "Lipstick" }))} href="/">
        Lipstick 
      </a>
      <a className="menu-item" onClick={() => dispatch(productList({ type: "Face-Powder" }))} href="/">
        Face-Powder 
      </a>
    </Menu>
  );
};