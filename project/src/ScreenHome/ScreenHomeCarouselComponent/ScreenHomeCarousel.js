import React from "react";
import "slick-carousel/slick/slick.css";  
import "slick-carousel/slick/slick-theme.css";  
import pic1 from "../../img/CarouselImg/nyka1.jpg"
import pic2 from "../../img/CarouselImg/nyka2.gif"
import pic3 from "../../img/CarouselImg/nyka3.jpeg"
import pic4 from "../../img/CarouselImg/nyka4.gif"
import pic5 from "../../img/CarouselImg/nyka5.gif"
import pic6 from "../../img/CarouselImg/nyka6.jpg"
import Slider from "react-slick";
import "./ScreenHomeCarousel.css"
const ScreenHomeCarousel = () => {
  const settings = {
    dots: true,  
    infinite: true,  
    autoplay:true,
    speed: 500,  
    centerMode: true,  
    slidesToShow: 1,  
    slidesToScroll: 1
  }
    
  return (
    <div className="container-fluid"> 
      <div  className=" _slider_nykaa" data-ride="carousel">
        <Slider {...settings}>
          <img className='image1' src={pic1} alt='image1' />
          <img className='image2' src={pic2} alt='image2' />
          <img className='image1' src={pic1} alt='image1' />
          <img className='image4' src={pic4} alt='image4' />
          <img className='image5' src={pic5} alt='image5' />
          <img className='image3' src={pic3} alt='image3' />
          <img className='image6' src={pic6} alt='image6' />
        </Slider>
      </div>
       
      {/* <div class="_product" data-ride="carousel">
                    <img className='image2' src={pic2} alt='image2' />
                </div>
                <div class="_product" data-ride="carousel">
                    <img className='image5' src={pic5} alt='image5' />
                </div> */}
    </div> 
        
           
  )

}

export default ScreenHomeCarousel