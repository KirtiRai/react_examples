import React from "react"
import "./ScreenHomeTopBrand.css"
import { Link } from "react-router-dom"
const ScreenHomeTopBrands = ({brand}) => {
  return (
    <div className='col-14 col-md-6 col-lg-6 mx-0 mb-4'>
      <div className="card p-0 overflow-hidden h-100 shadow">
      <img className="card-img-top img-fluid" src={brand.img} alt={"img"}/>
        <div className="card-body">
        <Link to="/brand">
           <div className="titles-description-sticky">
          <h5 className="card-title">{brand.title}</h5>
          <h5 className="card-desc">{brand.desc}</h5>
          </div>
          </Link> 
     </div>
    </div>
    </div>
  )
}

export default ScreenHomeTopBrands