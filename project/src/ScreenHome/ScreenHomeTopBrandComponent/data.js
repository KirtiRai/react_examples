import top1 from "../../img/TopBrand/top1.jpg"
import top2 from "../../img/TopBrand/top2.jpg"
import top3 from "../../img/TopBrand/top3.webp"
import top4 from "../../img/TopBrand/top4.webp"
import top5 from "../../img/TopBrand/top5.jpg"
import top6 from "../../img/TopBrand/top8.webp"
import top7 from "../../img/TopBrand/topbrand2.jpg"
import top8 from "../../img/TopBrand/topbrand3.jpg"
const TopBrand=[
    {
        id:1,
        img:top1,
        title:"ON Rs.4000:Free Blush(Worth Rs.2500)",
        desc:"On Rs.3000:2 Gifts"
    },
    {
        id:2,
        img:top2,
        title:"On Rs.2000:Free Blush(Worth Rs.500)",
        desc:"Upto 10% Off"


    },
    {
        id:3,
        img:top3,
        title:"FREE Summer Travel Kit Worth Rs. 399",
        desc:"On Rs.449 & Above + Upto 40% off"


    },
    {
        id:4,
        img:top4,
        title:"On Rs. 1500: Skin Tint",
        desc:"On Rs.3000: 3-Piece Kit"


    },
    {
        id:5,
        img:top5,
        title:"Upto 33% Off",
        desc:"Buy 5 Get 3 On SheetMasks!*"


    },
    {
        id:6,
        img:top6,
        title:"Upto 40% Off",
        desc:"Minimizes Pores & 8 HR Oil Control"


    },
    {
        id:7,
        img:top7,
        title:"Upto 30% Off",
        desc:"FREE Mini Shampoo On Orders Over Rs.399"


    },
    {
        id:8,
        img:top8,
        title:"Upto 50% Off + ",
        desc:"Complimentary Lipstick On Orders Above Rs. 999"

    }
]
export default TopBrand