import React,{useEffect, useRef}from "react"
import NavBarLower from "../CommonComponent/NavBarLowerComponent/NavBarLower"
import NavBarMiddle from "../CommonComponent/NavBarMiddleComponent/NavBarMiddle"
import NavBarUpper from "../CommonComponent/NavBarUpperComponent/NavBarUpper"
import ScreenHomeCarousel from "../ScreenHome/ScreenHomeCarouselComponent/ScreenHomeCarousel"
import ScreenHomeFeatureBrands from "./ScreenHomeFeatureBrandsComponent/ScreenHomeFeatureBrands"
import ScreenHomeEditorsChoice from "./ScreenHomeEditorsChoiceComponent/ScreenHomeEditorsChoice"
import ScreenHomeTopBrands from "./ScreenHomeTopBrandComponent/ScreenHomeTopBrands"
import Footer from "../CommonComponent/FooterComponent/Footer"
import "./ScreenHome.css"
import TopBrand from "./ScreenHomeTopBrandComponent/data"
import Cart from "../ScreenBrand/Cart"
import Sidebar from "../ScreenBrand/Sidebar"
const ScreenHome = () => {
  const topContainer = useRef();

  useEffect(() => {
    topContainer.current.scrollIntoView({ block: "end", behavior: 'smooth' });
    }, []);
  
  return (
    <div>
      <div ref={topContainer} />
      <NavBarUpper />
      <NavBarMiddle />
      <NavBarLower />
      {/* <Sidebar /> */}
     <Cart />
      <div><ScreenHomeCarousel /></div>
      <div style={{ marginTop: "2%" }}>
        <div className='heading'>FEATURE BRANDS</div>
        <div className='flex-container'>
          <div><ScreenHomeFeatureBrands /></div>
          <div><ScreenHomeFeatureBrands /></div>
          <div><ScreenHomeFeatureBrands /></div>
          <div><ScreenHomeFeatureBrands /></div>
          <div><ScreenHomeFeatureBrands /></div>
          <div><ScreenHomeFeatureBrands /></div>
          <div><ScreenHomeFeatureBrands /></div>
          <div><ScreenHomeFeatureBrands /></div>
        </div>
      </div>
      <div style={{ marginTop: "2%" }}>
        <div className='heading'>EDITOR'S CHOICE</div>
        <div className='flex-container'>
          <div><ScreenHomeEditorsChoice /></div>
          <div><ScreenHomeEditorsChoice /></div>
          <div><ScreenHomeEditorsChoice /></div>
          <div><ScreenHomeEditorsChoice /></div>
          <div><ScreenHomeEditorsChoice /></div>
          <div><ScreenHomeEditorsChoice /></div>
          <div><ScreenHomeEditorsChoice /></div>
          <div><ScreenHomeEditorsChoice /></div>
        </div>
      </div>
      <div style={{ marginTop: "2%" }}>
        <div className='heading'>TOP BRANDS</div>
        <section className="py-4 container">
          <div className="row justify-content-center">
            {TopBrand.map((brand) => {
              return (
                <ScreenHomeTopBrands key={brand.id} brand={brand} />
              )

            })}
          </div>
        </section>
      </div>
      <Footer />
    </div>
    
  )
}

export default ScreenHome
