import React from "react"
import { Card, CardImg, CardText, CardBody, CardTitle } from "reactstrap"
import SampleImg from "../../img/Card/SignUpGiftBox.jpg"
import "./ScreenHomeFeatureBrands.css"

const ScreenHomeFeatureBrands = (props) => {
  return (
    <div>
      <Card>
        <CardImg top src={SampleImg} alt="Card image cap" />
        <div className='card-flex'>
          <div className='card-color'>On Rs.3000 Pick 3 Gifts!</div>
          <div className='para-color'>Choose From Makeup & Skincare Minis!</div>
        </div>
      </Card>
    </div>
  )
}

export default ScreenHomeFeatureBrands
