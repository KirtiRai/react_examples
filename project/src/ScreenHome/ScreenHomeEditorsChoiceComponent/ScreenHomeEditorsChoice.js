import React from "react"
import { Card, CardImg, CardText, CardBody, CardTitle } from "reactstrap"
import SampleImg from "../../img/Card/SignUpGiftBox.jpg"
import "./ScreenHomeEditorsChoice.css"

const ScreenHomeEditorsChoice = (props) => {
  return (
    <div>
      <Card>
        <CardImg top src={SampleImg} alt="Card image cap" />
        <div className='card-flex'>
          <div className='card-heading'>Sweat-Proof Makeup</div>
          <div className='card-para'>Expert Tips For an All-Day Look!</div>
        </div>
      </Card>
    </div>
  )
}

export default ScreenHomeEditorsChoice
