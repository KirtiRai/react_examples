// import { SELECTION_COLUMN } from "antd/lib/table/hooks/useSelection";
import { ADD_TO_CART, REMOVE_TO_CART, SHOW_HIDE_CART } from "../Types";
const CartReducer = (state, action) => {
  switch (action.type){
  case ADD_TO_CART:{
    return {
      ...state,
      cartItems:[...state.cartItems, action.payload]
    }
  }
  case SHOW_HIDE_CART:{
    return {
      ...state,
      showCart: !state.showCart
      
    }
  }
  case REMOVE_TO_CART:{
    return {
      ...state,
      cartItems: state.cartItems.filter(item=>item.id!==action.payload)
      
    }
  }
  default:
    return state
  }
}
export default CartReducer