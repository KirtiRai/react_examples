/* eslint-disable react/react-in-jsx-scope */
import React, { useReducer} from "react";
import CartContext from "./CartContext";
import CartReducer from "./CartReducer";
import { ADD_TO_CART,SHOW_HIDE_CART,REMOVE_TO_CART } from "../Types";
const CartState = ({children}) => {
  const initialState = {
    cartItems:[],
    showCart:false

  }
  const [state, dispatch] = useReducer(CartReducer, initialState);
  const addToCart = item => {
    dispatch({
      type:ADD_TO_CART,
      payload:item

    })
  }

  const showHideCart = () => {
    dispatch({
      type:SHOW_HIDE_CART,
    })
  }

  const removeToCart = (id) => {
    dispatch({
      type:REMOVE_TO_CART,
      payload:id
    })

  }
  return (
    <CartContext.Provider value={{
      cartItems: state.cartItems,
      showCart: state.showCart,
      addToCart,
      showHideCart,
      removeToCart
    }}>{children}
    </CartContext.Provider>

  )

}
export default CartState