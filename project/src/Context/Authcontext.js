import React, { useContext, useState, useEffect } from "react"
// import { useEffect } from 'react/cjs/react.production.min'
import { auth } from "../firebase"

const AuthContext = React.createContext()

export const useAuth = () => {
  return useContext(AuthContext)
}

export const AuthProvider = ({children}) => {
  const [currentUser, setCurrentUser] = useState("")

  const signUp = (email, password) => {
    auth.createUserWithEmailAndPassword(email, password)
  }

  useEffect(() => {
    const unSubscribe = auth.onAuthStateChange(user => {
      setCurrentUser(user)
    })
    return unSubscribe
  }, [])
  
  const value = {
    currentUser,
    signUp
  }
  
  return (
    <AuthContext.Provider value={value}>
      {children}
    </AuthContext.Provider>
  )
}
